﻿using System.Linq;
using System.Text;
using System.Collections;
using System.IO;
using System.Text.Json;
using Newtonsoft.Json.Linq;
#nullable disable

namespace DataNormalize
{
    class Program
    {   
        
        static void Main(string[] args)
        {
            List<Hotel> hotels = new List<Hotel>();

            // Get Old JSON
            StreamReader r = new StreamReader("data/hotels.json");
            string json = r.ReadToEnd();

            hotels = JsonSerializer.Deserialize<List<Hotel>>(json);
            
            var hotel_list = hotels.GroupBy(p => p.HotelId).Select((hotel) => 
            {
                return new{ HotelId = hotel.Key, 
                            Price = hotel.Min(a => a.Price), 
                            Guarantee = hotel.Any(b => b.Guarantee==true)};
            });
            
            // New JSON
            var ex_json = JsonSerializer.Serialize(hotel_list.ToList());

            Console.WriteLine(ex_json);
           
        }
    }

     public class Hotel
     {
         public int HotelId { get; set; }
         public long Price { get; set; }
         public bool Guarantee { get; set; }
     }
}